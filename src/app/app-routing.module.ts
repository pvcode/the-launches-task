import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LaunchesComponent } from '@components/launches/launches.component';

import { SingleRocketComponent } from '@components/rockets/rocket.component';
import { PageNotFoundComponent } from '@components/other/404/pagenotfound.component';


const routes: Routes = [

{ path: '', pathMatch: 'full', redirectTo: 'all' },
{ path: 'all', component: LaunchesComponent, data: { show: 'all', title: 'All launches' } },
{ path: 'upcoming', component: LaunchesComponent, data: { show: 'upcoming', title: 'Upcoming Launches' } },
{ path: 'past', component: LaunchesComponent, data: { show: 'past', title: 'Past launches' }},

 { path: 'rocket/:id', 
     component: SingleRocketComponent,
     data: {
     	title: "Rocket"
    }

   },

 {path: '404', component: PageNotFoundComponent},
 {path: '**', redirectTo: '/404'}



];




@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }


