import { Component, OnInit } from '@angular/core';
import { LaunchService } from '@app/services/data.service';
import { ActivatedRoute } from '@angular/router';
import { PageNotFoundComponent } from '@components/other/404/pagenotfound.component';

@Component({
  selector: 'single-rocket',
  templateUrl: './rocket.component.html',
  styleUrls: ['./rocket.component.css']
})


export class SingleRocketComponent implements OnInit {

  title!: string;
  id!: string;
  rocket!: any;
  error!: boolean;
  isLoading: boolean;


  constructor(private activatedRoute: ActivatedRoute, private dataService: LaunchService) {

  	  this.id = this.activatedRoute.snapshot.params['id'];
  	  this.error = false;
  	  this.isLoading = false;
  }


  ngOnInit() {

	this.getSingleRocket();
  
  }


   getSingleRocket(): void {

   	  let rocketId = this.id;
   	  this.isLoading = true;

   	  setTimeout(() => {

      this.dataService.getRocket(rocketId).subscribe(rocket => {


          this.rocket = rocket;

          this.isLoading = false;

        },  err => this.error = true);

  		},900);
  }



  goBack() {
  	window.history.back();
  }


}