import {
  Component,
  OnInit,
  DoCheck
} from '@angular/core';
import {
  DEFAULT_PARAMS,
  Params,
  Orders
} from '@models/params.model';
import {
  Launch
} from '@models/launch.model';
import {
  LaunchService
} from '@app/services/data.service';
import {
  ActivatedRoute,
  Router,
  NavigationEnd
} from '@angular/router';

@Component({
  selector: 'app-launches',
  templateUrl: './launches.component.html',
  styleUrls: ['./launches.component.css']
})



export class LaunchesComponent implements OnInit {

  title!: string;
  blankRows!: number[];
  isLoading!: boolean;
  launches!: Launch[];
  math = Math;
  max: number | undefined;
  params: Params;
  show: string;
  oldParamsOrder!: Orders;
  oldParamsOffset!: number;
  render: string;
  willLaunch: boolean;


  constructor(private launchService: LaunchService, private route: ActivatedRoute) {
    this.blankRows = [];
    this.isLoading = false;
    this.max = undefined;
    this.params = DEFAULT_PARAMS;
    this.show = '';
    this.render = '';
    this.willLaunch = false;
  }


  ngOnInit() {
    this.render = this.route.snapshot.data.show;

    this.params.offset = 0;

    this.title = document.title;

    switch (this.render) {
      case 'all': {
        this.getLaunches();
        break;
      }
      case 'upcoming': {
        this.getUpcomingLaunches();
        break;
      }

      case 'past': {

        this.getPastLaunches();
        break;
      }

    }

  }

  getLaunches(): void {

    this.isLoading = true;

      /* NGX-SKELETON LOADER TESTING */

    setTimeout(() => {

      this.launchService.getAllLaunches(this.params)
        .subscribe(launches => {
          this.launches = launches;
          this.isLoading = false;

          if (launches.length < this.params.limit) {
            this.max = this.params.limit + launches.length;
          }
        });


    }, 900);

  }



  getUpcomingLaunches(): void {

    this.isLoading = true;

      /* NGX-SKELETON LOADER TESTING */

    setTimeout(() => {

      this.launchService.getUpcomingLaunches(this.params)
        .subscribe(launches => {
          this.launches = launches;

          this.isLoading = false;
          if (launches.length < this.params.limit) {
            this.max = this.params.offset + launches.length;
          }
        });


    }, 900);



  }


  getPastLaunches(): void {

    this.isLoading = true;

      /* NGX-SKELETON LOADER TESTING */

    setTimeout(() => {

      this.launchService.getPastLaunches(this.params)
        .subscribe(launches => {
          this.launches = launches;

          this.isLoading = false;
          if (launches.length < this.params.limit) {
            this.max = this.params.offset + launches.length;
          }
        });


    }, 900);



  }


  diffBetweenDates(data: any) {

    let launchDate = new Date(data);
    let dateNow = new Date();

    let diffInMilliSeconds = Math.abs(dateNow.getTime() - launchDate.getTime()) / 1000;

    let daysBetweenDates = Math.floor((launchDate.getTime() - dateNow.getTime()) / 1000 / 60 / 60 / 24);

    // calculate days
    const days = Math.floor(diffInMilliSeconds / 86400);
    diffInMilliSeconds -= days * 86400;

    // calculate hours
    const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
    diffInMilliSeconds -= hours * 3600;

    // calculate minutes
    const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
    diffInMilliSeconds -= minutes * 60;

    let difference = 'Starts in: ';

    if (days > 0) {
      difference += (days === 1) ? `${days} day, ` : `${days} days, `;
    }

    if (daysBetweenDates < 0) {
      difference = '';
    } else {

      this.willLaunch = true;

      difference += (hours === 0 || hours === 1) ? `${hours} hour, ` : `${hours} hours, `;

      difference += (minutes === 0 || hours === 1) ? `${minutes} minutes` : `${minutes} minutes`;

    }
    return difference;

  }

  handleOffset(offset: number): void {

    this.params.offset = offset;
    window.scrollTo(0,0);

    switch (this.render) {
      case 'all': {
        this.getLaunches();
        break;
      }
      case 'upcoming': {
        this.getUpcomingLaunches();
        break;
      }

      case 'past': {

        this.getPastLaunches();
        break;
      }

    }

  }
}