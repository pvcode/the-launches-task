import { Component , OnInit } from '@angular/core';  
import { Title } from '@angular/platform-browser';  
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';  
import { filter, map } from 'rxjs/operators'; 


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {


/* SET PAGE TITLE BASED ON DECLARATION IN ROUTES MODULE */

	constructor(private router: Router,  
        private activatedRoute: ActivatedRoute,  
        private titleService: Title) { 
	
	}  
  



  ngOnInit() {
    const appTitle = this.titleService.getTitle();
    this.router
      .events.pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => {
          const child = this.getChild(this.activatedRoute);

          if (child.snapshot.data['title']) {
            return child.snapshot.data['title'];
          }
          return appTitle;
        })
      ).subscribe((ttl: string) => {
        this.titleService.setTitle(ttl);
      });
 
}
	/* I think : any here is not good practice, but I'm a little bit lost with this part, need to dig deeper and read more about this */

  getChild(activatedRoute: ActivatedRoute): any {  
    if (activatedRoute.firstChild) {  
      return this.getChild(activatedRoute.firstChild);  
    } else {  
      return activatedRoute;  
    }  
  }
}

