import { BrowserModule, Title} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { LaunchesComponent } from '@components/launches/launches.component';
import { SingleRocketComponent } from '@components/rockets/rocket.component';
import { PageNotFoundComponent } from '@components/other/404/pagenotfound.component';

import { HeaderComponent } from '@components/header/header.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';


@NgModule({
  declarations: [
    AppComponent,
    LaunchesComponent,
    HeaderComponent,
    SingleRocketComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxSkeletonLoaderModule,
    AppRoutingModule
 ],
  providers: [Title],
  bootstrap: [AppComponent]
})
export class AppModule { }