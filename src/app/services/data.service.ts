import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Launch } from '@models/launch.model';
import { Params, rocketParams } from '@models/params.model';
import { Utils } from './utilities.service';


@Injectable({
  providedIn: 'root'
})



export class LaunchService {

  AlllaunchesUrl = 'https://api.spacexdata.com/v3/launches';
  UpcomingaunchesUrl = 'https://api.spacexdata.com/v3/launches/upcoming';
  PastlaunchesUrl = 'https://api.spacexdata.com/v3/launches/past';
  singleRocketUrl = 'https://api.spacexdata.com/v3/rockets'

  constructor(private http: HttpClient) {}

  getAllLaunches(params: Params): Observable<Launch[]> {
    const queryString = Utils.getQueryString(params);
    return this.http.get<Launch[]>(`${this.AlllaunchesUrl}?${queryString}`)
      .pipe();
  }

  getUpcomingLaunches(params: Params): Observable<Launch[]> {
    const queryString = Utils.getQueryString(params);
    return this.http.get<Launch[]>(`${this.UpcomingaunchesUrl}?${queryString}`)
      .pipe();
  }

  getPastLaunches(params: Params): Observable<Launch[]> {
    const queryString = Utils.getQueryString(params);
    return this.http.get<Launch[]>(`${this.PastlaunchesUrl}?${queryString}`)
      .pipe();
  }


	getRocket(rocketId: string): Observable<any> {
	   return this.http.get<any>(`${this.singleRocketUrl}/${rocketId}`);
	}



}