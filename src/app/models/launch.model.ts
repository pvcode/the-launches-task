class Links {
  wikipedia!: string | null;
  mission_patch!: string | null;
}

class Rocket {
  rocket_name!: string;
  rocket_id!: string;
}

export class Launch {
  details!: string;
  flight_number!: number;
  launch_year!: string;
  links!: Links;
  rocket!: Rocket;
  mission_patch!: Links;
  mission_name!: string;
  launch_date_utc!: string;
  launch_success!: boolean;
  upcoming!: boolean;
}
