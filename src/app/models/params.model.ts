export type Orders = 'asc' | 'desc';

export class Params {
  limit!: number;
  offset!: number;
  order!: Orders;
  sort!: 'launch_year';
  [key: string]: string | number;
}


export class rocketParams {
	rocketId!: string;
}

export const DEFAULT_PARAMS: Params = {
  limit: 15,
  offset: 0,
  order: 'desc',
  sort: 'launch_year',
};